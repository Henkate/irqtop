/*
 * Copyright (C) 2013-2017 Elboulangero <elboulangero@gmail.com>
 *
 * This file is part of irqtop.
 *
 * irqtop is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irqtop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with irqtop. If not, see <http://www.gnu.org/licenses/>.
 */

/* ISO C */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

/* POSIX */
#include <unistd.h>
#include <getopt.h>
#include <fcntl.h>
#include <dirent.h>
#include <termios.h>

/* Local */
#include "log.h"
#include "list.h"
#include "kthread.h"
#include "interrupt.h"

#define CLEAR "\033[2J\033[1;1H"
#define BOLD_START  "\033[1m"
#define BOLD_STOP   "\033[0m"

/*
 * Global variables
 */
unsigned int cpu_count;
unsigned int loop_count;
unsigned int elapsed_time;

/*
 * Options
 */
static int display_active_only;
static int display_irq_only;
static int display_total_only;
static unsigned int period;

static void
display_options(void)
{
	printf("irqtop displays the number of interrupts for a given period.\n"
	       "This period can be specified with the -p option (default: 1 second).\n"
	       "\n"
	       "Options:\n"
	       "-a --active    Display active interrupts only.\n"
	       "-i --irq       Display IRQs only (hide Linux internal interrupts).\n"
	       "-p --period=T  Set the period between two screen updates, in seconds.\n"
	       "-t --total     Do not display info for each CPU, only display a total.\n"
	       "-h --help      Print this help message.\n"
	       "-v --version   Print the program version.\n");
}

static void
display_version(void)
{
	printf("irqtop 0.3.1 - (C) 2013-2017 Elboulangero <elboulangero@gmail.com>\n"
	       "Released under the GNU GPLv3.\n");
}

static void
display_usage(void)
{
	display_version();
	printf("\n");
	display_options();
}

static void
options_init(int argc, char *argv[])
{
	int c;
	int help;
	int version;
	int error;
	struct option long_opt[] = {
		{"active", no_argument, NULL, 'a'},
		{"irq", no_argument, NULL, 'i'},
		{"period", required_argument, NULL, 'p'},
		{"total", no_argument, NULL, 't'},
		{"help", no_argument, NULL, 'h'},
		{"version", no_argument, NULL, 'v'},
		{NULL, 0, NULL, 0}
	};

	help = 0;
	version = 0;
	error = 0;
	period = 1;

	while ((c = getopt_long(argc, argv, "a"	/*      --active        */
				"i"	/*      --irq           */
				"p:"	/*      --period        */
				"t"	/*      --total         */
				"h"	/*      --help          */
				"v",	/*      --version       */
				long_opt, NULL)) != EOF) {
		switch (c) {
		case 'a':
			display_active_only = 1;
			break;
		case 'i':
			display_irq_only = 1;
			break;
		case 'p':
			period = strtoul(optarg, NULL, 10);
			break;
		case 't':
			display_total_only = 1;
			break;
		case 'h':
			help = 1;
			break;
		case 'v':
			version = 1;
			break;
		default:
			error = 1;
		}
	}

	if (error) {
		display_usage();
		exit(EXIT_FAILURE);
	}

	if (help) {
		display_usage();
		exit(EXIT_SUCCESS);
	}

	if (version) {
		display_version();
		exit(EXIT_SUCCESS);
	}
}

/*
 * Get the number of CPUs.
 */
static unsigned int
get_cpu_count(void)
{
	FILE *fp;
	char line[256];
	char *ptr;
	int count = 0;

	/* Open file */
	fp = fopen("/proc/interrupts", "r");
	if (!fp) {
		error("Failed to open %s", "/proc/interrupts");
		return 0;
	}

	/* Get first line */
	if (!fgets(line, sizeof line, fp)) {
		error("Failed to read %s", "/proc/interrupts");
		goto close_and_return;
	}

	/* Get the total number of CPU */
	ptr = line;
	while ((ptr = strstr(ptr, "CPU"))) {
		count++;
		ptr += 3;
	}

 close_and_return:
	fclose(fp);
	return count;
}

/*
 * List of kernel threads managing IRQs.
 * These threads only exist if the kernel is running with the option "threadirqs".
 * They're found by browsing all the PID directories in /proc.
 */
LIST_HEAD(kernel_irq_thread_list);

static void
refresh_kthreads(void)
{
	DIR *dir;
	struct dirent *dir_entry;
	struct kthread *entry;
	static unsigned int last_refresh_time = UINT_MAX;

	/* Refresh only once per second */
	if (last_refresh_time != elapsed_time) {
		debug("Refreshing kernel threads list at time %u", elapsed_time);
		last_refresh_time = elapsed_time;
	} else {
		return;
	}

	/* Cleanup kthread list */
	list_for_each_entry(entry, &kernel_irq_thread_list, list) {
		kthread_free(entry);
	}
	list_del_init(&kernel_irq_thread_list);

	/* Open /proc directory */
	dir = opendir("/proc");
	if (!dir) {
		error("Failed to open dir %s", "/proc");
		return;
	}

	/* Fill kthread list by browsing /proc */
	while ((dir_entry = readdir(dir)) != NULL) {
		char *endptr;
		unsigned long pid;
		struct kthread *kthread;

		/* We're only interested in directories */
		if (!(dir_entry->d_type & DT_DIR)) {
			continue;
		}

		/* Check if the directory name is a PID */
		pid = strtoul(dir_entry->d_name, &endptr, 10);
		if (*endptr != '\0') {
			continue;
		}

		/* Try to create a kernel thread for this PID.
		 * If the process referenced by the PID is not an IRQ kernel thread,
		 * the function will just fail.
		 */
		kthread = kthread_new(pid);
		if (kthread) {
			list_add(&kthread->list, &kernel_irq_thread_list);
		}
	}

	/* Close /proc */
	closedir(dir);
}

/*
 * List of interrupts
 */
LIST_HEAD(interrupt_list);	// persistent between each iteration
LIST_HEAD(active_interrupt_list);	// rebuilt at each iteration

static int
refresh_interrupts(void)
{
	FILE *fp;
	char line[256];
	size_t line_count;
	struct list_head *list_start = NULL;

	/* Reset active interrupt list */
	list_del_init(&active_interrupt_list);

	/* Open file */
	fp = fopen("/proc/interrupts", "r");
	if (!fp) {
		error("Failed to open %s", "/proc/interrupts");
		return -1;
	}

	/* Skip first line */
	if (!fgets(line, sizeof line, fp)) {
		error("Failed to get first line");
		goto close_on_error;
	}

	/* Update every interrupt in the list */
	line_count = 0;
	list_start = &interrupt_list;
	while (fgets(line, sizeof line, fp)) {
		char *ptr;
		size_t id_len;
		struct list_head *list;
		struct interrupt *entry;

		/* Get interrupt ID for this line */
		ptr = strchr(line, ':');
		if (!ptr) {
			error("Invalid line: %s", line);
			goto close_on_error;
		}
		id_len = ptr - line;

		/* Browse the list to find the entry corresponding to this line */
		list_for_each_until(list, list_start, &interrupt_list) {
			entry = list_entry(list, struct interrupt, list);
			if (strncmp(line, entry->id, id_len) == 0) {
				interrupt_refresh(entry, line, loop_count);
				break;
			}
		}

		/* The interrupt may not be in the list, either because we're just starting
		 * and the list is empty, or because the interrupt was just registered in
		 * the kernel.
		 * Anyway, in both cases we create it and add it to the list.
		 */
		if (list == &interrupt_list) {
			debug("line[%.3s] is a new entry", line);

			/* Refresh kernel threads list, it may have changed */
			refresh_kthreads();

			/* Create the interrupt */
			entry = interrupt_new(line, &kernel_irq_thread_list, loop_count);
			if (!entry) {
				error("Failed to create interrupt !");
				return -1;
			}

			/* Insert in the list at the right position */
			list_add(&entry->list, list_start);
		}

		/* Add it to the active interrupts list if the current rate is not zero.
		 * This list is sorted by rate, from the most active to the least active.
		 */
		if (entry->total.rate.cur != 0) {
			struct interrupt *new_entry = entry;

			/* Browse the active list, insert and stop as soon as we find an entry
			 * with a lower rate than us.
			 */
			list_for_each(list, &active_interrupt_list) {
				entry = list_entry(list, struct interrupt, list);
				if (new_entry->total.rate.cur > entry->total.rate.cur) {
					list_add_tail(&new_entry->active_list, list);
					break;
				}
			}

			/* Check whether we inserted the entry */
			if (list == &active_interrupt_list) {
				list_add_tail(&new_entry->active_list, list);
			}
		}

		/* Increment stuff */
		++line_count;
		list_start = list_start->next;
	}

	/* Now, ensure that every interrupt in the list has been refreshed.
	 * If an interrupt has not been refreshed, it's because it was not found in
	 * /proc/interrupts, which means it has been unregistered in the kernel.
	 * In this case we remove it from the list.
	 */
	{
		struct list_head *list, *tmp;
		struct interrupt *entry;

		list_for_each_safe(list, tmp, &interrupt_list) {
			entry = list_entry(list, struct interrupt, list);
			if (entry->iter_cur != loop_count) {
				debug("entry[%s] has been unregistered", entry->id);
				list_del(list);
				interrupt_free(entry);
			}
		}
	}

	fclose(fp);
	return 0;

 close_on_error:
	fclose(fp);
	return -1;
}

static const char *
duration_to_string(unsigned int duration)
{
	static char buf[16 + 1];
	unsigned int seconds;
	unsigned int minutes;
	unsigned int hours;

	seconds = duration % 60;
	duration /= 60;
	minutes = duration % 60;
	duration /= 60;
	hours = duration;

	snprintf(buf, sizeof buf, "%02u:%02u:%02u", hours, minutes, seconds);
	return buf;
}

static void
print_interrupts(void)
{
	struct interrupt *entry;

	/* First lines */
	printf("Irqtop - running for %s (period: %u, loops: %u) - CPUs: %u\n",
	       duration_to_string(elapsed_time), period, loop_count, cpu_count);
	printf("Toggle hotkeys - a: active, i: irq, t: total\n");
	printf("Quit hotkeys - q: quit, s: quit and print session summary\n");

	/* Title */
	printf(BOLD_START);

	printf("   INTERRUPTS            TOTAL RATES  ");
	if (!display_total_only) {
		size_t i;
		size_t len = cpu_count * 7;
		len -= 9;
		for (i = 0; i < len; i++) {
			printf(" ");
		}
		printf("RATES/CPU  ");
	}
	printf("           ");
	printf("\n");

	printf(" ID   PID PRI      MIN   LAST    MAX  ");
	if (!display_total_only) {
		size_t i;

		for (i = 0; i < cpu_count; i++) {
			printf("   CPU%zu", i);
		}
		printf("  ");
	}
	printf("      SUM   DEVICE NAME");
	printf("\n");

	printf(BOLD_STOP);

	/* Interrupts */
	if (display_active_only) {
		list_for_each_entry(entry, &active_interrupt_list, active_list) {
			if (!display_irq_only || entry->type == INT_IRQ) {
				interrupt_print(entry, display_total_only);
			}
		}
	} else {
		list_for_each_entry(entry, &interrupt_list, list) {
			if (!display_irq_only || entry->type == INT_IRQ) {
				interrupt_print(entry, display_total_only);
			}
		}
	}
}

static void
summarize_interrupts(void)
{
	struct interrupt *entry;

	printf("Irqtop session summary - duration: %s, period: %u, loops: %u - CPUs: %u\n",
	       duration_to_string(period * loop_count), period, loop_count, cpu_count);

	/* Title line 1 */
	printf("   INTERRUPTS            TOTAL RATES  ");
	if (!display_total_only) {
		size_t i;
		size_t len = cpu_count * 7;
		len -= 14;
		for (i = 0; i < len; i++) {
			printf(" ");
		}
		printf("MEAN RATES/CPU  ");
	}
	printf("           ");
	printf("\n");

	/* Title line 2 */
	printf(" ID   PID PRI      MIN   MEAN    MAX  ");
	if (!display_total_only) {
		size_t i;

		for (i = 0; i < cpu_count; i++) {
			printf("   CPU%zu", i);
		}
		printf("  ");
	}
	printf("      SUM   DEVICE NAME");
	printf("\n");

	/* Interrupts */
	list_for_each_entry(entry, &interrupt_list, list) {
		if (entry->total.rate.max == 0) {
			/* We don't display interrupts that were never active
			 * during the session.
			 */
			continue;
		}
		if (!display_irq_only || entry->type == INT_IRQ) {
			interrupt_print_summary(entry, display_total_only);
		}
	}
}

/*
 * Terminal
 */
static int
term_init(struct termios *save)
{
	struct termios new;

	/* Save current terminal parameters */
	tcgetattr(fileno(stdin), save);

	/* Copy to a new struct */
	new = *save;

	/* Set terminal to non-canonical mode */
	new.c_lflag &= ~(ICANON);

	/* Disable echo */
	new.c_lflag &= ~ECHO;

	/* Set attributes */
	tcsetattr(fileno(stdin), TCSANOW, &new);

	/* Start new screen */
	system("command -v tput >/dev/null 2>&1 && tput smcup");

	return 0;
}

static int
term_restore(struct termios *term)
{
	/* Restore screen */
	system("command -v tput >/dev/null 2>&1 && tput rmcup");

	/* Set attributes */
	tcsetattr(fileno(stdin), TCSANOW, term);

	return 0;
}

/*
 * Main
 */
int
main(int argc, char *argv[])
{
	int err = 0;
	int loop = 1;
	int print_summary = 0;
	struct termios term;

	/* Init global stuff */
	cpu_count = get_cpu_count();
	options_init(argc, argv);
	if (cpu_count == 1) {
		display_total_only = 1;
	}

	/* Init kthread list */
	refresh_kthreads();

	/* Setup terminal */
	term_init(&term);

	/* NON blocking stdin */
	fcntl(fileno(stdin), F_SETFL, O_NONBLOCK);

	/* Loop */
	loop_count = UINT_MAX;
	elapsed_time = 0;
	while (loop) {
		char key;

		/* Look for user input */
		if (read(fileno(stdin), &key, 1) == 1) {
			switch (key) {
				/* Toggle hotkeys */
			case 'a':
				display_active_only = !display_active_only;
				break;
			case 'i':
				display_irq_only = !display_irq_only;
				break;
			case 't':
				if (cpu_count != 1) {
					display_total_only = !display_total_only;
				}
				break;
				/* Quit hotkeys */
			case 'q':
				loop = 0;
				break;
			case 's':
				print_summary = 1;
				loop = 0;
				break;
			default:
				break;
			}
		}

		/* Flush */
		while (read(fileno(stdin), &key, 1) > 0) {
		}

		/* Refresh interrupts */
		if (elapsed_time % period == 0) {
			loop_count++;
			err = refresh_interrupts();
		}
		if (err) {
			break;
		}

		/* Print interrupts */
		printf(CLEAR);
		print_interrupts();

		/* Exit if needed */
		if (!loop) {
			break;
		}

		/* Let's have a little nap */
		sleep(1);
		elapsed_time++;
	}

	/*  Restore screen */
	term_restore(&term);

	/* Error checking */
	if (err) {
		error("Exiting because of error");
		return EXIT_FAILURE;
	}

	/* Print summary */
	if (print_summary) {
		summarize_interrupts();
	}

	return EXIT_SUCCESS;
}
