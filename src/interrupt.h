/*
 * Copyright (C) 2013-2017 Elboulangero <elboulangero@gmail.com>
 *
 * This file is part of irqtop.
 *
 * irqtop is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irqtop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with irqtop. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _IRQTOP_INTERRUPT_H_
#define _IRQTOP_INTERRUPT_H_

#include "list.h"
#include "kthread.h"

/*
 * Interrupt types
 */
enum interrupt_type {
	INT_UNKNOWN,
	INT_IRQ,		/*      IRQ                             */
	INT_INTERNAL		/*      kernel internal interrupt       */
};

/*
 * Counter keeps track of how many interrupts happened.
 *  cur:  current interrupt count.
 *  prev: previous interrupt count.
 *  sum:  sum of all interrupts since irqtop was started.
 */
struct counter {
	unsigned long cur;
	unsigned long prev;
	unsigned long sum;
};

/*
 * Rates are defined as number of interruts per 'period'.
 * 'period' is user defined.
 *  cur: current interrupt rate.
 *  min: minimal rate recorded since irqtop was started.
 *  max: maximal rate recorded since irqtop was started.
 */
struct rate {
	unsigned long cur;
	unsigned long min;
	unsigned long max;
};

/*
 * CPU struct containing counter and rates.
 *  count: counters.
 *  rate : rates.
 */
struct cpu {
	struct counter count;
	struct rate rate;
};

/*
 * Object for interrupt monitoring.
 *  id     : the ID for this interrupt.
 *  device : the name of the device that registered this interrupt.
 *  type   : the type of this interrupt.
 *  kthread: kernel thread associated with this interrupt, if any.
 *  cpu    : counters and rates for each CPU.
 *  total  : total counters and rates.
 *  iter_start: the number of the iteration at the creation of this interrupt.
 *  iter_cur: the number of the last iteration (updated in interrupt_refresh()).
 *  list   : list pointer, for external usage.
 *  active_list: same thing.
 */
struct interrupt {
	/* Identity */
	char id[3 + 1];
	char device[64 + 1];
	enum interrupt_type type;
	/* IRQ */
	struct kthread *kthread;
	/* Status, ie counters, rates and iter */
	struct cpu *cpu;
	struct cpu total;
	/* Iterations that happened */
	unsigned int iter_start;
	unsigned int iter_cur;
	/* Lists */
	struct list_head list;
	struct list_head active_list;
};

struct interrupt *interrupt_new(const char *line, struct list_head *kthread_list,
				unsigned int iter_count);
void interrupt_free(struct interrupt *self);
void interrupt_refresh(struct interrupt *self, const char *line, unsigned int iter_count);
void interrupt_print(struct interrupt *self, int total_only);
void interrupt_print_summary(struct interrupt *self, int total_only);

#endif				/* _IRQTOP_INTERRUPT_H_ */
