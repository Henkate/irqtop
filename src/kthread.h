/*
 * Copyright (C) 2013-2017 Elboulangero (elboulangero@gmail.com)
 *
 * This file is part of irqtop.
 *
 * irqtop is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irqtop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with irqtop. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _IRQTOP_KTHREAD_H_
#define _IRQTOP_KTHREAD_H_

#include "list.h"

/*
 * Kernel thread object.
 * On realtime kernels, there's a kernel thread associated with each IRQ.
 *  pid : kernel thread PID.
 *  prio: realtime priority for this thread.
 *  irq : IRQ managed by this thread.
 *  list: list pointer, for external usage.
 */
struct kthread {
	unsigned int pid;
	int prio;
	unsigned int irq;
	/* List */
	struct list_head list;
};

struct kthread *kthread_new(unsigned int pid);
void kthread_free(struct kthread *self);
struct kthread *kthread_dup(struct kthread *src);

#endif				/* _IRQTOP_KTHREAD_H_ */
